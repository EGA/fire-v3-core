package uk.ac.ebi.ega.fire.utils;

import org.apache.commons.io.input.CountingInputStream;
import org.apache.commons.io.input.ProxyInputStream;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import uk.ac.ebi.ega.fire.exceptions.ClientProtocolException;
import uk.ac.ebi.ega.fire.models.ErrorResponse;

import java.io.IOException;
import java.io.InputStream;

import static uk.ac.ebi.ega.fire.utils.FireUtils.getObjectMapper;

public class RetryingDownloadStream extends CountingInputStream {

    private final long rangeStart;
    private final long rangeEnd;
    private final String requestURL;
    private final CloseableHttpClient httpClient;

    public RetryingDownloadStream(CloseableHttpClient httpClient, String requestURL, long rangeStart, long rangeEnd) throws ClientProtocolException {
        super(null);
        this.requestURL = requestURL;
        this.rangeStart = rangeStart;
        this.rangeEnd = rangeEnd;
        this.httpClient = httpClient;
        this.reconnect();
    }

    @Override
    public int read() throws IOException {
        while (true) {
            try {
                return super.read();
            } catch (IOException e) {
                try {
                    reconnect();
                } catch (ClientProtocolException ex) {
                    throw new IOException(ex);
                }
            }
        }
    }

    @Override
    public int read(byte[] b) throws IOException {
        while (true) {
            try {
                return super.read(b);
            } catch (IOException e) {
                try {
                    reconnect();
                } catch (ClientProtocolException ex) {
                    throw new IOException(ex);
                }
            }
        }
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        while (true) {
            try {
                return super.read(b, off, len);
            } catch (IOException e) {
                try {
                    reconnect();
                } catch (ClientProtocolException ex) {
                    throw new IOException(ex);
                }
            }
        }
    }

    private void reconnect() throws ClientProtocolException {

        if (this.in != null) {
            try {
                this.in.close();
            } catch (IOException e) {
                // Do nothing
            }
        }

        final HttpUriRequest request = RequestBuilder
                .get(requestURL)
                .addHeader("Range", String.format("bytes=%d-%d", rangeStart + this.getByteCount(), rangeEnd))
                .build();

        while (true) {
            final CloseableHttpResponse httpResponse;
            try {
                httpResponse = httpClient.execute(request);

                final int status = httpResponse.getStatusLine().getStatusCode();
                if (status >= 200 && status < 300) {
                    this.in = new RequestClosingStream(httpResponse.getEntity().getContent(), httpResponse);
                    return;
                }
                try (CloseableHttpResponse errorResponse = httpResponse){
                    throw new ClientProtocolException(getObjectMapper().readValue(
                            EntityUtils.toString(errorResponse.getEntity()), ErrorResponse.class));
                }
            } catch (IOException e) {
                // Do nothing and loop to try again
            }
        }

    }

    private static class RequestClosingStream extends ProxyInputStream {

        private final CloseableHttpResponse httpResponse;

        public RequestClosingStream(InputStream proxy, CloseableHttpResponse httpResponse) {
            super(proxy);
            this.httpResponse = httpResponse;
        }

        @Override
        public void close() throws IOException {
            super.close();
            httpResponse.close();
        }
    }

}
