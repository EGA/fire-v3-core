/*
 *
 * Copyright 2019 EMBL - European Bioinformatics Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package uk.ac.ebi.ega.fire.exceptions;

import uk.ac.ebi.ega.fire.models.ErrorResponse;

/**
 * This exception is used in case of bad request.
 * Caller should not retry file upload if receives this exception.
 */
public class ClientProtocolException extends Exception {
    private final ErrorResponse errorResponse;

    public ClientProtocolException(final ErrorResponse errorResponse) {
        super(errorResponse.getDetail());
        this.errorResponse = errorResponse;
    }

    public int getStatusCode() {
        return errorResponse.getStatusCode();
    }

    public String getStatusMessage() {
        return errorResponse.getStatusMessage();
    }

    public String getDetails() {
        return errorResponse.getDetail();
    }
}
