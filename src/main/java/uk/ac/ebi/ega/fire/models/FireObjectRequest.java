/*
 *
 * Copyright 2019 EMBL - European Bioinformatics Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package uk.ac.ebi.ega.fire.models;

import java.io.File;
import java.io.FileNotFoundException;

/**
 * Model class to map FIRE request parameters.
 */
public class FireObjectRequest {
    private final File fileToUpload;
    private final String md5;
    private final String firePath;

    public FireObjectRequest(final File fileToUpload, final String md5,
                             final String firePath) throws FileNotFoundException {

        if (!fileToUpload.exists()) {
            throw new FileNotFoundException("File to upload ".concat(fileToUpload.getAbsolutePath()).concat(" doesn't exists"));
        }

        this.fileToUpload = fileToUpload;
        this.md5 = md5;
        this.firePath = firePath;
    }

    public File getFileToUpload() {
        return fileToUpload;
    }

    public String getMd5() {
        return md5;
    }

    public String getFirePath() {
        return firePath;
    }

    @Override
    public String toString() {
        return "FireObjectRequest{" +
                "fileToUpload=" + fileToUpload.getAbsolutePath() +
                ", md5='" + md5 + '\'' +
                ", firePath='" + firePath + '\'' +
                '}';
    }
}
