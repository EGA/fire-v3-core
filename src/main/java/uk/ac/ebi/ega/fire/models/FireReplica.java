/*
 *
 * Copyright 2019 EMBL - European Bioinformatics Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package uk.ac.ebi.ega.fire.models;

public class FireReplica implements IFireReplica {
    private long objectId;
    private String replicaId;
    private long objectSize;
    private String writeTime;
    private IFireDataStore dataStore;

    public FireReplica() {

    }

    @Override
    public long getObjectId() {
        return this.objectId;
    }

    public void setObjectId(long objectId) {
        this.objectId = objectId;
    }

    @Override
    public String getReplicaId() {
        return this.replicaId;
    }

    public void setReplicaId(String replicaId) {
        this.replicaId = replicaId;
    }

    @Override
    public long getObjectSize() {
        return this.objectSize;
    }

    public void setObjectSize(long objectSize) {
        this.objectSize = objectSize;
    }

    @Override
    public String getWriteTime() {
        return this.writeTime;
    }

    public void setWriteTime(String writeTime) {
        this.writeTime = writeTime;
    }

    @Override
    public IFireDataStore getDataStore() {
        return this.dataStore;
    }

    public void setDataStore(IFireDataStore dataStore) {
        this.dataStore = dataStore;
    }

}
