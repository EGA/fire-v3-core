/*
 *
 * Copyright 2019 EMBL - European Bioinformatics Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package uk.ac.ebi.ega.fire.models;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static uk.ac.ebi.ega.fire.utils.FireUtils.toFireURI;

public class FireResponse implements IFireResponse {

    private final FireObjectResponse fireObjectResponse;

    public FireResponse(final FireObjectResponse fireObjectResponse) {
        this.fireObjectResponse = fireObjectResponse;
    }

    @Override
    public long getObjectId() {
        return fireObjectResponse.getObjectId();
    }

    @Override
    public String getFireOid() {
        return fireObjectResponse.getFireOid();
    }

    @Override
    public String getMd5() {
        return fireObjectResponse.getObjectMd5();
    }

    @Override
    public long getObjectSize() {
        return fireObjectResponse.getObjectSize();
    }

    @Override
    public String getCreateTime() {
        return fireObjectResponse.getCreateTime();
    }

    @Override
    public Optional<List<KeyValue>> getMetadata() {
        return !fireObjectResponse.getMetadata().isEmpty()
                ? Optional.of(Collections.unmodifiableList(fireObjectResponse.getMetadata()))
                : Optional.empty();
    }

    @Override
    public Optional<String> getPath() {
        return fireObjectResponse.getFilesystemEntry() != null ? Optional.of(toFireURI(fireObjectResponse.getFilesystemEntry().getPath())) : Optional.empty();
    }

    @Override
    public boolean isPublished() {
        return fireObjectResponse.getFilesystemEntry() != null && fireObjectResponse.getFilesystemEntry().isPublished();
    }
}
