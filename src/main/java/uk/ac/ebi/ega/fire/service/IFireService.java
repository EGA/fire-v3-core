/*
 *
 * Copyright 2019 EMBL - European Bioinformatics Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package uk.ac.ebi.ega.fire.service;

import uk.ac.ebi.ega.fire.exceptions.ClientProtocolException;
import uk.ac.ebi.ega.fire.exceptions.FireServiceException;
import uk.ac.ebi.ega.fire.listener.ProgressListener;
import uk.ac.ebi.ega.fire.models.FireObjectRequest;
import uk.ac.ebi.ega.fire.models.FireResponse;
import uk.ac.ebi.ega.fire.models.IFireReplica;
import uk.ac.ebi.ega.fire.models.IFireResponse;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.FileAlreadyExistsException;
import java.util.List;
import java.util.Optional;

public interface IFireService {

    boolean deleteByPath(String firePath) throws FireServiceException, ClientProtocolException;

    boolean deleteById(String fireOid) throws ClientProtocolException, FireServiceException;

    Optional<FireResponse> findFile(String firePath) throws FireServiceException, ClientProtocolException;

    /**
     * @param fireObjectRequest FireObjectRequest object contains details of file to upload.
     * @param progressListener  Implementation of ProgressListener to get progress of bytes processed/transferred.
     * @throws FireServiceException       will be thrown when issue occurs while uploading file e.g. network issue.
     * @throws ClientProtocolException    will be thrown when issue in request e.g. invalid data sent.
     * @throws FileAlreadyExistsException will be thrown if file already exists on fire.
     */
    IFireResponse upload(final FireObjectRequest fireObjectRequest, final ProgressListener progressListener) throws FireServiceException, FileAlreadyExistsException, ClientProtocolException;

    List<IFireReplica> findReplicas(String fireOid) throws FireServiceException, ClientProtocolException;

    boolean isReplicaOnTapeStorage(String fireOid) throws ClientProtocolException, FireServiceException;

    /**
     * @param fireOid    fire ID of the object to download.
     * @param rangeStart the first byte to download.
     * @param rangeEnd   the last byte to download.
     * @return Stream of the downloaded bytes.
     */
    InputStream downloadByteRangeById(String fireOid, long rangeStart, long rangeEnd) throws FireServiceException, ClientProtocolException, FileNotFoundException, URISyntaxException;

    /**
     * @param firePath   fire Path of the object to download.
     * @param rangeStart the first byte to download.
     * @param rangeEnd   the last byte to download.
     * @return Stream of the downloaded bytes.
     */
    InputStream downloadByteRangeByPath(String firePath, long rangeStart, long rangeEnd) throws FireServiceException, ClientProtocolException, FileNotFoundException, URISyntaxException;
}
