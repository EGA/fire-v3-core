/*
 *
 * Copyright 2019 EMBL - European Bioinformatics Institute
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package uk.ac.ebi.ega.fire.service;

import org.apache.commons.io.IOUtils;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.config.ConnectionConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicHeader;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ebi.ega.fire.exceptions.ClientProtocolException;
import uk.ac.ebi.ega.fire.exceptions.FireServiceException;
import uk.ac.ebi.ega.fire.models.FireObjectRequest;
import uk.ac.ebi.ega.fire.models.IFireReplica;
import uk.ac.ebi.ega.fire.models.IFireResponse;
import uk.ac.ebi.ega.fire.properties.HttpClientProperties;

import javax.xml.bind.DatatypeConverter;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.nio.file.FileAlreadyExistsException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@Ignore("This test should only be run while developing the code because it accesses the real Fire service.")
public class FireServiceTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(FireServiceTest.class);

    private static IFireService fireService;

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    private static final String TEST_FIRE_DIR = "test-dir-tmp-";
    private static final String TEST_FILE = "test-file-to-upload-";

    @BeforeClass
    public static void setupTest() throws IOException {
        HttpClientProperties httpClientProperties = readClientProperties();
        Properties p = new Properties();
        p.load(FireServiceTest.class.getResourceAsStream("/application-test.properties"));

        String credentials = p.getProperty("fire.user") + ":" + p.getProperty("fire.key");
        final String base64EncodedCredentials = Base64.getEncoder().encodeToString(credentials.getBytes());

        final ConnectionConfig connectionConfig = ConnectionConfig.custom()
                .setBufferSize(httpClientProperties.getBufferSize())
                .build();

        final PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();
        connectionManager.setMaxTotal(httpClientProperties.getMaxTotal());
        connectionManager.setDefaultMaxPerRoute(httpClientProperties.getDefaultMaxPerRoute());

        final RequestConfig requestConfig = RequestConfig.custom()
                .setConnectTimeout(httpClientProperties.getTimeout() * 1000)
                .setConnectionRequestTimeout(httpClientProperties.getTimeout() * 1000)
                .setSocketTimeout(httpClientProperties.getTimeout() * 1000)
                .build();

        final CloseableHttpClient httpClient = HttpClients.custom()
                .setDefaultConnectionConfig(connectionConfig)
                .setConnectionManager(connectionManager)
                .setDefaultRequestConfig(requestConfig)
                .setDefaultHeaders(Collections.singleton(new BasicHeader("Authorization", "Basic ".concat(base64EncodedCredentials))))
                .build();
        fireService = new FireService(httpClient, p.getProperty("fire.url"));
    }

    private static HttpClientProperties readClientProperties() throws IOException {
        Properties p = new Properties();
        p.load(FireServiceTest.class.getResourceAsStream("/application-test.properties"));
        HttpClientProperties httpClientProperties = new HttpClientProperties();
        httpClientProperties.setBufferSize(Integer.parseInt(p.getProperty("httpclient.connection.buffer-size")));
        httpClientProperties.setDefaultMaxPerRoute(Integer.parseInt(
                p.getProperty("httpclient.connection.default-max-per-route")));
        httpClientProperties.setMaxTotal(Integer.parseInt(p.getProperty("httpclient.connection.max-total")));
        httpClientProperties.setTimeout(Integer.parseInt(p.getProperty("httpclient.connection.timeout")));
        return httpClientProperties;
    }

    /**
     * Tests for successful file upload to server.
     */
    @Test
    public void upload_WhenPassValidRequestData_ThenUploadFileToFIRE() throws IOException, NoSuchAlgorithmException, FireServiceException, ClientProtocolException {
        final byte[] content = "This is a test file to upload on FIRE".getBytes();
        final File tmpFileToUpload = createFileWithContent(TEST_FILE + UUID.randomUUID().toString(), content);

        final MessageDigest messageDigest = getMD5MessageDigest();
        messageDigest.update(content);

        final String md5 = getMD5(messageDigest);
        final FireObjectRequest fireObjectRequest = new FireObjectRequest(tmpFileToUpload, md5, TEST_FIRE_DIR.concat(tmpFileToUpload.getName()));

        final IFireResponse fireSuccessResponse = fireService.upload(fireObjectRequest, bytesTransferred -> LOGGER.info("bytes transferred={}", bytesTransferred));
        assertions(fireSuccessResponse, tmpFileToUpload);
    }

    /**
     * Tests for error from server when wrong MD5 pass.
     */
    @Test
    public void upload_WhenPassWrongMD5InRequestData_thenReceivesErrorResponse() throws IOException, FireServiceException {
        final byte[] content = "This is a test file to upload on FIRE".getBytes();
        final File tmpFileToUpload = createFileWithContent(TEST_FILE + UUID.randomUUID().toString(), content);
        final FireObjectRequest fireObjectRequest = new FireObjectRequest(tmpFileToUpload, "WrongMD=75a1e608e6f1cee5a7d8e3d0", TEST_FIRE_DIR.concat(tmpFileToUpload.getName()));

        try {
            fireService.upload(fireObjectRequest, bytesTransferred -> LOGGER.info("bytes transferred={}", bytesTransferred));
        } catch (ClientProtocolException cpe) {
            assertEquals(400, cpe.getStatusCode());
            assertNotNull(cpe.getStatusMessage());
            assertNotNull(cpe.getDetails());
        }
    }

    /**
     * Tests the successful multiple upload in parallel. Check for HttpClient thread safety.
     */
    @Test
    public void upload_WhenPassTwoFilesToUpload_ThenUploadsFilesToFIRE() throws IOException, NoSuchAlgorithmException, ExecutionException, InterruptedException {
        final byte[] content1 = "This is a test file1 to upload on FIRE".getBytes();
        final File tmpFileToUpload1 = createFileWithContent(TEST_FILE + UUID.randomUUID().toString(), content1);

        final MessageDigest messageDigest1 = getMD5MessageDigest();
        messageDigest1.update(content1);

        final String md51 = getMD5(messageDigest1);

        final FireObjectRequest fireObjectRequest1 = new FireObjectRequest(tmpFileToUpload1, md51, TEST_FIRE_DIR.concat(tmpFileToUpload1.getName()));

        final byte[] content2 = "This is a test file2 to upload on FIRE. File will be uploaded in parallel.".getBytes();
        final File tmpFileToUpload2 = createFileWithContent(TEST_FILE + UUID.randomUUID().toString(), content2);

        final MessageDigest messageDigest2 = getMD5MessageDigest();
        messageDigest2.update(content2);

        final String md52 = getMD5(messageDigest2);

        final FireObjectRequest fireObjectRequest2 = new FireObjectRequest(tmpFileToUpload2, md52, TEST_FIRE_DIR.concat(tmpFileToUpload2.getName()));

        final ExecutorService executor = Executors.newFixedThreadPool(2);

        final Future<Boolean> future1 = executor.submit(() -> {
            try {
                final IFireResponse fireSuccessResponse = fireService.upload(fireObjectRequest1, bytesTransferred -> LOGGER.info("File1 bytes transferred={}", bytesTransferred));
                assertions(fireSuccessResponse, tmpFileToUpload1);
                return true;
            } catch (FireServiceException | ClientProtocolException e) {
                LOGGER.error("Error while uploading test file.", e);
                return false;
            }
        });

        final Future<Boolean> future2 = executor.submit(() -> {
            try {
                final IFireResponse fireSuccessResponse = fireService.upload(fireObjectRequest2, bytesTransferred -> LOGGER.info("File2 bytes transferred={}", bytesTransferred));
                assertions(fireSuccessResponse, tmpFileToUpload2);
                return true;
            } catch (FireServiceException | ClientProtocolException e) {
                LOGGER.error("Error while uploading test file.", e);
                return false;
            }
        });

        assertTrue(future1.get());
        assertTrue(future2.get());
        executor.shutdown();
    }

    /**
     * Test for duplicate file upload.
     */
    @Test(expected = FileAlreadyExistsException.class)
    public void upload_WhenPassSameRequestDataTwice_ThenReceivesConflictError() throws IOException, NoSuchAlgorithmException, FireServiceException, ClientProtocolException {
        final byte[] content = "This is a test file to upload on FIRE".getBytes();
        final File tmpFileToUpload = createFileWithContent(TEST_FILE + UUID.randomUUID().toString(), content);

        final MessageDigest messageDigest = getMD5MessageDigest();
        messageDigest.update(content);

        final String md5 = getMD5(messageDigest);
        final FireObjectRequest fireObjectRequest = new FireObjectRequest(tmpFileToUpload, md5, TEST_FIRE_DIR.concat(tmpFileToUpload.getName()));

        final IFireResponse fireSuccessResponse = fireService.upload(fireObjectRequest, bytesTransferred -> LOGGER.info("bytes transferred={}", bytesTransferred));
        assertions(fireSuccessResponse, tmpFileToUpload);

        fireService.upload(fireObjectRequest, bytesTransferred -> LOGGER.info("bytes transferred={}", bytesTransferred));
    }

    private void assertions(final IFireResponse fireSuccessResponse, final File file) {
        assertNotNull(fireSuccessResponse);
        assertEquals(file.length(), fireSuccessResponse.getObjectSize());
        assertNotNull(fireSuccessResponse.getCreateTime());
        assertTrue(fireSuccessResponse.getObjectId() > 0);
        assertNotNull(fireSuccessResponse.getFireOid());
        assertFalse(fireSuccessResponse.isPublished());
        assertTrue(fireSuccessResponse.getPath().isPresent());
        assertEquals("fire://".concat(TEST_FIRE_DIR.concat(file.getName())), fireSuccessResponse.getPath().get());
    }

    private File createFileWithContent(final String fileName, final byte[] content) throws IOException {
        final File tmpFileToUpload = temporaryFolder.newFile(fileName);
        try (final OutputStream outputStream = new FileOutputStream(tmpFileToUpload)) {
            outputStream.write(content);
            outputStream.flush();
        }
        return tmpFileToUpload;
    }

    private MessageDigest getMD5MessageDigest() throws NoSuchAlgorithmException {
        return MessageDigest.getInstance("MD5");
    }

    private String getMD5(final MessageDigest messageDigest) {
        return DatatypeConverter.printHexBinary(messageDigest.digest()).toLowerCase();
    }

    @Test
    public void testFindFileDoesNotExistReturnsEmpty() throws FireServiceException, ClientProtocolException {
        assertFalse(fireService.findFile("dev/doesNotExist").isPresent());
    }

    @Test
    public void testDeleteNotExistingFileReturnsFalse() throws FireServiceException, ClientProtocolException {
        boolean status = fireService.deleteByPath("dev/doesNotExist.txt");
        assertFalse(status);
    }

    @Test
    public void testFindReplicasDoesNotExistReturnsEmpty() throws FireServiceException, ClientProtocolException {
        assertTrue(fireService.findReplicas("dev/doesNotExist").isEmpty());
    }

    @Test
    @Ignore("This test is ignored because the replicas can only be tested in production env.")
    public void testFindReplicasFilesWithReplicasReturnsReplicas() throws FireServiceException, ClientProtocolException {
        List<IFireReplica> replicas = fireService.findReplicas("bdd61387aa02728f74ab002b05361e09");
        assertEquals(3, replicas.size());
        assertEquals(984887, replicas.get(0).getObjectId());
        assertEquals(161321025, replicas.get(0).getObjectSize());
        assertFalse(replicas.get(0).getDataStore().getName().isEmpty());
    }

    @Test
    @Ignore("This test is ignored because the replicas can only be tested in production env.")
    public void testIsReplicaOnTapeStorage() throws FireServiceException, ClientProtocolException {
        assertTrue(fireService.isReplicaOnTapeStorage("bdd61387aa02728f74ab002b05361e09"));
    }

    @Test
    public void testCanDownloadByteRangeOfFileById() throws IOException, FireServiceException, ClientProtocolException, URISyntaxException {
        // Ask the fireService to download a particular byte range from a known test file
        try (InputStream stream = fireService.downloadByteRangeById("93a7c9c1a9634d5ca2d46f2a1529933f", 5, 29)) {
            // Check that the bytes we got back are what we expected
            Assert.assertEquals("is a test file1 to upload", IOUtils.toString(stream));
        }
    }

    @Test
    public void testCanDownloadByteRangeOfFileByPath() throws IOException, FireServiceException, ClientProtocolException, URISyntaxException {
        // Ask the fireService to download a particular byte range from a known test file
        try (InputStream stream = fireService.downloadByteRangeByPath("/another-test-dir/another-test-file", 0, 4)) {
            // Check that the bytes we got back are what we expected
            Assert.assertEquals("hello", IOUtils.toString(stream));
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void testWhenByteRangeIsInvalidThrowsException() throws FireServiceException, ClientProtocolException, FileNotFoundException, URISyntaxException {
        fireService.downloadByteRangeById("93a7c9c1a9634d5ca2d46f2a1529933f", 5, 2900);
    }
}
