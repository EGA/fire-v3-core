package uk.ac.ebi.ega.fire.utils;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.CountingInputStream;
import org.apache.http.ProtocolVersion;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicStatusLine;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import uk.ac.ebi.ega.fire.exceptions.ClientProtocolException;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class RetryingDownloadStreamTest {

    @Test
    public void testWhenConnectionIsLostDownloadIsResumed() throws IOException, ClientProtocolException {

        final BasicStatusLine successStatus = new BasicStatusLine(new ProtocolVersion("HTTP", 1, 0), 200, "");

        final String content = "takoyaki-hosomaki";
        final int cutoff = 5;

        // Create a CountingInputStream where we make it throw an exception after bytes are read, to simulate the
        // connection being lost
        CountingInputStream brokenStream = new CountingInputStream(IOUtils.toInputStream(content)) {
            @Override
            public int read() throws IOException {
                if (this.getByteCount() >= cutoff) {
                    throw new IOException();
                }
                return super.read();
            }
        };

        // Create a CloseableHttpResponse from our broken stream
        CloseableHttpResponse brokenResponse = mock(CloseableHttpResponse.class);
        when(brokenResponse.getEntity()).thenReturn(new InputStreamEntity(brokenStream));
        when(brokenResponse.getStatusLine()).thenReturn(successStatus);

        // Create another CloseableHttpResponse for the retry
        CloseableHttpResponse retryResponse = mock(CloseableHttpResponse.class);
        when(retryResponse.getEntity()).thenReturn(new StringEntity(content.substring(cutoff)));
        when(retryResponse.getStatusLine()).thenReturn(successStatus);

        // Create a mock ClosableHttpClient and configure it so that when we call execute() on it the first time, we get
        // the first HttpResponse, and when we call it the second time, we get the second HttpResponse
        CloseableHttpClient mockClient = mock(CloseableHttpClient.class);
        when(mockClient.execute(any()))
                .thenReturn(brokenResponse)
                .thenReturn(retryResponse);

        // Create a RetryingDownloadStream using our mockClient and request all the bytes
        try (InputStream stream = new RetryingDownloadStream(mockClient, "", 0, content.length())) {
            // Check that reading the stream combines the data from the requests in the way we expect
            // Read the stream one byte at a time so that it uses the right code in our broken stream
            // Expect five characters of the first response followed by the second response
            assertEquals(content, readStreamToStringOneByteAtATime(stream));
        }

        // Check that two requests were made and capture them
        ArgumentCaptor<HttpUriRequest> requestArgumentCaptor = ArgumentCaptor.forClass(HttpUriRequest.class);
        verify(mockClient, times(2)).execute(requestArgumentCaptor.capture());
        List<HttpUriRequest> requests = requestArgumentCaptor.getAllValues();

        // Check that the requests had the right ranges
        // The first one should have been for the whole content
        assertEquals(String.format("bytes=0-%d", content.length()), requests.get(0).getFirstHeader("Range").getValue());
        // The second one should start from where we cut off
        assertEquals(String.format("bytes=%d-%d", cutoff, content.length()), requests.get(1).getFirstHeader("Range").getValue());

        // Check that both CloseableHttpResponse objects were closed
        verify(brokenResponse).close();
        verify(retryResponse).close();
    }

    private String readStreamToStringOneByteAtATime(InputStream stream) throws IOException {
        StringWriter writer = new StringWriter();
        while (true) {
            int b = stream.read();
            if (b == -1) break;
            writer.write(b);
        }
        return writer.toString();
    }

}